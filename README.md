# Setup

You can get a working development environement and regenerate all the files used in the talk either by using docker or guix.

## Using docker

Simply run the following:
```bash
docker compose run --rm shell
cd sources
bash full_build.sh
```

If for some reason you don't have docker compose, it's only used to conveniently mount the source code, you can run a similar container using the following docker run command:
```bash
docker run --rm -v `pwd`:/home/bidouille/sources -it registry.gitlab.inria.fr/viroulea/bidouille-instrumentation:llvm-14 /bin/bash
```

## Using Guix

Run the following:

```bash
guix shell --manifest=manifest.scm -- bash
bash full_build.sh
```

If your guix is not up to date or if you want exactly the same commit I used, I emitted the channels I used that you can provide to guix pull like this:
```bash
guix pull -C channels.scm
```


# Output

The few interesting files we will look at, in both the `build` and the `build-pgo` folder:
  - `pass_inline.txt`: the optimization report for inlining
  - `pass_unrolll.txt`: the optimization report for unrolling
  - `optimized.ll`: the LLVM IR for the build
  - `optimized.S`: the assembly for the build
