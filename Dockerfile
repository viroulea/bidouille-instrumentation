ARG LLVM_VERSION=14
ARG DEBIAN_VERSION=bullseye
FROM --platform=linux/amd64 debian:${DEBIAN_VERSION}
ARG LLVM_VERSION
ARG DEBIAN_VERSION

ARG DEBIAN_FRONTEND=noninteractive

SHELL ["/bin/bash", "-c"]

RUN apt-get update
RUN apt-get install -yqq --no-install-recommends \
  bc \
  binutils \
  binutils-dev \
  build-essential \
  ca-certificates \
  cmake \
  git \
  g++ \
  gcc \
  gnupg \
  lsb-release \
  python3-pip \
  software-properties-common \
  sudo \
  unzip \
  vim \
  wget

RUN pip install lcov_cobertura

#RUN wget https://apt.llvm.org/llvm.sh
RUN wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add -
RUN echo "deb http://apt.llvm.org/${DEBIAN_VERSION}/ llvm-toolchain-${DEBIAN_VERSION}-${LLVM_VERSION} main" | tee /etc/apt/sources.list.d/llvm.list
RUN apt-get update
RUN apt-get install -yqq --no-install-recommends -t "llvm-toolchain-${DEBIAN_VERSION}-${LLVM_VERSION}" \
  clang-${LLVM_VERSION} \
  clang-tools-${LLVM_VERSION} \
  llvm-${LLVM_VERSION}-dev \
  llvm-${LLVM_VERSION}-tools \
  libclang-rt-${LLVM_VERSION}-dev

RUN update-alternatives --install /usr/bin/clang clang /usr/bin/clang-${LLVM_VERSION} 100
RUN update-alternatives --install /usr/bin/clang++ clang++ /usr/bin/clang++-${LLVM_VERSION} 100

RUN apt-get autoremove -yqq

# Create the bidouille user
RUN useradd -m bidouille
# Don't do this at home
RUN echo 'bidouille ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

USER bidouille
ENV SHELL=/bin/bash
WORKDIR /home/bidouille
