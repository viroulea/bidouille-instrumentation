#!/usr/bin/env bash
set -euxo pipefail

if [ ! -d build ]; then
  mkdir build
fi

if [ ! -d build-pgo ]; then
  mkdir build-pgo
fi
cmake . -B build -C caches/Instrumentation.cmake
cmake --build build --target profile
cmake . -B build-pgo -C caches/PGO.cmake -DBIDOUILLE_INSTR_PROFDATA=`pwd`/build/code.profdata
cmake --build build-pgo
