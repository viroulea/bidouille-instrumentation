;; What follows is a "manifest" equivalent to the command line you gave.
;; You can store it in a file that you may then pass to any 'guix' command
;; that accepts a '--manifest' (or '-m') option.

(specifications->manifest
  (list "llvm@14"
        "clang-runtime@14"
        "findutils"
        "grep"
        "cmake"
        "python"
        "python-lcov-cobertura"
        "coreutils"
        "bash"
        "make"
        "clang-toolchain@14"))
