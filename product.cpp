#include <algorithm>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <random>
#include <utility>
#include <vector>

using namespace std;

// This simply generates a random number if T is an int, uint64_t, or double.
//  - if T is an int, return a number between -3 and 3
//  - if T is an uint64_t, perform a dice roll
//  - if T is a double, return a real between 10 and 12
template <typename T> T roll() {
  static random_device Rd;
  static mt19937 Rng(Rd());
  if constexpr (is_same_v<T, int>) {
    static uniform_int_distribution<T> Dis{-3, 3};
    return Dis(Rng);
  } else if constexpr (is_same_v<T, unsigned long>) {
    static uniform_int_distribution<T> Dis{1, 6};
    return Dis(Rng);
  } else if constexpr (is_same_v<T, unsigned short>) {
    static uniform_int_distribution<T> Dis{10, 12};
    return Dis(Rng);
  } else {
    return T{};
  }
}

template <typename ContainerTy> void show(ContainerTy const &Container) {
  cout << "Elements(" << Container.size() << "):\n";
  bool First{true};
  for (auto &Elem : Container) {
    if (!First) {
      cout << ", ";
    }
    cout << Elem;
    First = false;
  }
  cout << "\n";
}

// Creates two vectors containing N random elements of type T
template <typename T>
pair<vector<T>, vector<T>> generateVectors(unsigned long N) {
  vector<T> Ref(N);
  ranges::generate(Ref, roll<T>);
  vector<T> Products(N);
  ranges::generate(Products, roll<T>);
  return make_pair(Ref, Products);
}

// Iterate over both vectors, and multiply each element in Ref by the
// corresponding one in Products.
template <typename T> vector<T> action(unsigned long N) {
  auto [Ref, Products] = generateVectors<T>(N);
  for (size_t i = 0; i < N; i++) {
    Ref[i] *= Products[i];
  }
  return std::move(Ref);
}

// prog taille algo
int main(int argc, char **argv) {

  if (argc < 3) {
    cerr << "TODO\n";
    return 1;
  }

  char *end = nullptr;

  unsigned long N = strtoul(argv[1], &end, 10);
  unsigned long Action = strtoul(argv[2], &end, 10);

  switch (Action) {
  case 1: {
    cout << "Action 1\n";
    cout << "Let's just emit some message :)\n";
  } break;
  case 2: {
    cout << "Action 2\n";
    auto Result = action<unsigned short>(N);
    show(Result);
  } break;
  case 24: {
    cout << "Action 24\n";
    auto Result = action<int>(N);
    show(Result);
  } break;
  case 42: {
    cout << "Action 42\n";
    auto Result = action<unsigned long>(N);
    show(Result);
  } break;
  default: {
    cerr << "No action\n";
  }
  }
  cout << "Size: " << N << "\n";

  return 0;
}
